#!/usr/bin/env gnuplot

set boxwidth 0.95 absolute
set xrange [0:75]
set style fill transparent solid 0.5 noborder

plot "/tmp/commitdata/greg@greghaynes.net-normalized" title "Gregory Haynes" with boxes, \
     "/tmp/commitdata/svenbrauch@googlemail.com-normalized" title "Sven Brauch" with boxes lc 3