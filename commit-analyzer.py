#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Example usage: python commit-analyzer.py ~/projects/code/ktp-*

import subprocess
import re
import copy
import sys
import os
import numpy

# dismiss all authors which have less than this many commits overall
MIN_CHANGE_COUNT = 4

class Changeset:
    def __init__(self, author, groups=None):
        self.author = author
        if groups is None:
            groups = (0, 0, 0)
        self.filesChanged = int(groups[0])
        self.insertions = int(groups[1])
        self.deletions = int(groups[2])
        self.changes = self.insertions + self.deletions

    def __iadd__(self, other):
        if self.author != other.author:
            self.author = "<different authors>"
        self.filesChanged += other.filesChanged
        self.insertions += other.insertions
        self.deletions += other.deletions
        self.changes += other.changes
        return self

    def __repr__(self):
        return '<Changeset({0} files changed, +{1}, -{2})>'.format(self.filesChanged, self.insertions, self.deletions)

class ProjectSummary():
    def __init__(self):
        self.summary = {}
        self.entries = []

    def authors(self):
        return self.summary.keys()

    def changesByAuthor(self, author):
        return [x for x in self.entries if x.author == author]

    def addEntry(self, author, logData):
        changeset = Changeset(author, logData)
        if author in self.summary.keys():
            self.summary[author] += changeset
        else:
            self.summary[author] = copy.deepcopy(changeset)
        self.entries.append(changeset)

    def parseShortlog(self, log):
        comma = log.find(',')
        if log.find('insertion') == -1:
            log = log[:comma] + "," + " 0 insertions(+)" + log[comma:]
        if log.find('deletion') == -1:
            log = log + ", 0 deletions(-)"
        match = re.match(r"\s*(\d*) files? changed, (\d*) insertions?\(\+\), (\d*) deletions?\(\-\)", log)
        try:
            return match.groups()
        except:
            print "Error parsing:", log

    def doProcessing(self):
        output = subprocess.check_output(["git", "log", '--format="%ce"', "--shortstat", "--find-copies-harder"])
        lines = filter(lambda x: x != "", output.splitlines())
        atAuthor = True
        for line in lines:
            # format is author, commitstat, author, commitstat etc
            if line.find("changed, ") == -1:
                author = line
                continue
            self.addEntry(author, self.parseShortlog(line))

def binDatapoints(points, normalize=False):
    result = dict()
    count = 0
    for point in points:
        count += 1
        if point in result.keys():
            result[point] += 1
        else:
            result[point] = 1
    if normalize:
        for key in result.keys():
            result[key] /= float(count)
    return result

def formatBinnedDatapoints(binned):
    result = str()
    keys = sorted(binned.keys())
    for key in keys:
        result += "{0}\t{1}\n".format(key, binned[key])
    return result

if __name__ == '__main__':
    projects = sys.argv[1:]
    cwd = os.getcwd()
    summaries = []
    try:
        os.mkdir("/tmp/commitdata")
    except OSError:
        pass
    for project in projects:
        os.chdir(cwd)
        os.chdir(project)
        s = ProjectSummary()
        s.doProcessing()
        summaries.append(s)

    authors = set.union(set(), *[a.authors() for a in summaries])
    for author in authors:
        changesets = []
        for summary in summaries:
            changesets.extend(summary.changesByAuthor(author))
        count = len(changesets)
        if count < MIN_CHANGE_COUNT:
            continue
        datapoints = [changeset.changes for changeset in changesets]
        changes = sum(datapoints)
        average = round(changes / float(count), 1)
        deviation = round(numpy.std(datapoints), 1)
        median = round(numpy.median(datapoints), 1)
        print "{0}:".format(author).ljust(60),
        print "average {0}±{1}, median {2}, total {3}".format(average, deviation, median, changes)

        for normalized in True, False:
            name = ("/tmp/commitdata/{0}" + ("-normalized" if normalized else "")).format(author.replace('"', ''))
            with open(name, 'w') as f:
                f.write("# Scanned projects: {0}; normalized: {1}\n".format(', '.join(projects), normalized))
                f.write(formatBinnedDatapoints(binDatapoints(datapoints, normalized)))

    print "Advanced information was written to /tmp/commitdata."
